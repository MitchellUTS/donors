package com.example.ses.navViewFragments;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.ses.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

public class otherProfileFragment extends Fragment{

    private Button donationHistoryButton;
    private FirebaseFirestore db;
    private FirebaseUser user;
    private NavigationView navView;
    private FirebaseAuth mAuth;
    private View currentView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_profile_no_edit, parent, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        Bundle bundle = this.getArguments();

        TextView name = view.findViewById(R.id.profileNameNoEdit);
        TextView phone = view.findViewById(R.id.profilePhoneNoEdit);
        TextView email = view.findViewById(R.id.profileEmailNoEdit);

        name.setText(bundle.getString("Name"));
        phone.setText(bundle.getString("Phone"));
        email.setText(bundle.getString("Email"));

    }

}