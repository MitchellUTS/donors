package com.example.ses.navViewFragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.ses.R;
import com.example.ses.data.DonationBooking;


public class DonateAndBook extends Fragment {
    private Button bookBtn;
    private Button donationButton;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().setTitle("Donate and Book");
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_donate_book,container,false);
        bookBtn = v.findViewById(R.id.book_btn);
        donationButton = v.findViewById(R.id.donateButton);

        bookBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Class fragmentClass = null;
                Fragment fragment = null;

                fragmentClass = DonationBooking.class;

                try {
                    fragment = (Fragment) fragmentClass.newInstance();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                FragmentManager fragmentManager = getFragmentManager();
                //fragmentManager.popBackStack();
                fragmentManager.beginTransaction().replace(R.id.mainLayout, fragment).addToBackStack(null).commit();
            }
        });

        donationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Class fragmentClass = null;
                Fragment fragment = null;

                fragmentClass = donationWindow.class;

                try {
                    fragment = (Fragment) fragmentClass.newInstance();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                FragmentManager fragmentManager = getFragmentManager();
                //fragmentManager.popBackStack();
                fragmentManager.beginTransaction().replace(R.id.mainLayout, fragment).addToBackStack(null).commit();
            }
        });
        return v;
    }
}
