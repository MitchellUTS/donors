package com.example.ses.navViewFragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.ses.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.GeoPoint;

import java.util.HashMap;
import java.util.Map;

public class registerCharityActivity extends Fragment{

    private FirebaseFirestore db;
    private FirebaseUser user;
    private NavigationView navView;
    private FirebaseAuth mAuth;
    private View currentView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState){

        return inflater.inflate(R.layout.activity_register_charity, parent, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        user = mAuth.getCurrentUser();

        final EditText charityNameEditText = view.findViewById(R.id.charityNameET);
        final EditText charityEmailEditText = view.findViewById(R.id.charityEmailET);
        final EditText charityLatitudeEditText = view.findViewById(R.id.charityLatitudeET);
        final EditText charityLongitudeEditText = view.findViewById(R.id.charityLongitudeET);
        final EditText charityBankNameEditText = view.findViewById(R.id.charityBankNameET);
        final EditText charityBankNoEditText = view.findViewById(R.id.charityBankNoET);
        final EditText charityBSBEditText = view.findViewById(R.id.charityBSBET);
        final EditText charityWebLinkEditText = view.findViewById(R.id.charityWebLinkET);
        final Button registerCharityButton = view.findViewById(R.id.registerCharityButton);

        //final ProgressBar loadingProgressBar = findViewById(R.id.loading);

        registerCharityButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isNumExistInString(charityNameEditText.getText().toString())){
                    Toast.makeText(getContext(), "Please do not use numbers for your full name.", Toast.LENGTH_LONG).show();
                    return;
                }

                if(isStringEqual(charityEmailEditText.getText().toString(), "")){
                    Toast.makeText(getContext(), "The email field is empty", Toast.LENGTH_LONG).show();
                    return;
                }

                if(isStringEqual(charityLatitudeEditText.getText().toString(), "")){
                    Toast.makeText(getContext(), "The location field is empty", Toast.LENGTH_LONG).show();
                    return;
                }

                if(isStringEqual(charityLongitudeEditText.getText().toString(), "")){
                    Toast.makeText(getContext(), "The location field is empty", Toast.LENGTH_LONG).show();
                    return;
                }

                if(isStringEqual(charityWebLinkEditText.getText().toString(), "")){
                    Toast.makeText(getContext(), "The Website field is empty", Toast.LENGTH_LONG).show();
                    return;
                }

                if(isStringEqual(charityBankNameEditText.getText().toString(), "")){
                    Toast.makeText(getContext(), "The Website field is empty", Toast.LENGTH_LONG).show();
                    return;
                }

                if(isStringEqual(charityBankNoEditText.getText().toString(), "")){
                    Toast.makeText(getContext(), "The Website field is empty", Toast.LENGTH_LONG).show();
                    return;
                }

                if(isStringEqual(charityBSBEditText.getText().toString(), "")){
                    Toast.makeText(getContext(), "The Website field is empty", Toast.LENGTH_LONG).show();
                    return;
                }


                //Create the account after fields have been checked.
                //Switch the activity to Login once they have created the account.
                createCharityAccount(charityNameEditText.getText().toString(), charityEmailEditText.getText().toString(),
                        Double.parseDouble(charityLatitudeEditText.getText().toString()), Double.parseDouble(charityLongitudeEditText.getText().toString()),
                        charityWebLinkEditText.getText().toString(), charityBankNameEditText.getText().toString(),
                        charityBankNoEditText.getText().toString(), charityBSBEditText.getText().toString());

                //Put text here that confirms charity registration
            }
        });

    }

    private void createCharityAccount(String name, String email, Double longitude, Double latitude, String webLink, String bankName, String bankNo, String bsb){
        final String TAG = "createAccountFunc ===";

        //Prepare fields for innerclass.
        final String innerclassName = name;
        final String innerclassEmail = email;
        final String innerclassWebLink = webLink;
        final String innerclassBankName = bankName;
        final String innerclassBankNo = bankNo;
        final String innerclassBSB = bsb;

        final GeoPoint gps = new GeoPoint(longitude, latitude);

        CollectionReference charities = db.collection("Charities");

        Log.d("registerCharity", "adding charity to database");
        //Prepare empty maps and data map
        //data map
        Map<String, Object> data = new HashMap<>();

        //Put data into the map
        data.put("Name", name);
        data.put("Location", gps);
        data.put("Email", email);
        data.put("WebLink", webLink);
        data.put("AccountName", bankName);
        data.put("AccountNo", bankNo);
        data.put("BSB", bsb);

        //send it to firebase.
        charities.document(user.getUid())
                .set(data)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void none) {

                        Toast.makeText(getContext(), "Charity has been created",
                                Toast.LENGTH_LONG).show();
                    }

                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getContext(), "Charity has failed to be created",
                                Toast.LENGTH_LONG).show();
                    }
                });
        Log.d("what the heck", "finished");


    }

    private boolean isNumExistInString(String input){
        char[] chars = input.toCharArray();
        for(char c : chars){
            if(Character.isDigit(c)){
                return true;
            }
        }
        return false;
    }

    private boolean isStringEqual(String s1, String s2){
        if(s1.equals(s2)){
            return true;
        }
        return false;
    }
}

