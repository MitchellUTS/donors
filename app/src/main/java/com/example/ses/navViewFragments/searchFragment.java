package com.example.ses.navViewFragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import com.example.ses.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class searchFragment extends Fragment {

    Switch optionSwitch;
    FirebaseFirestore db;
    FirebaseUser user;
    FirebaseAuth mAuth;
    List<QueryDocumentSnapshot> list = new ArrayList<>();
    List<QueryDocumentSnapshot> list2 = new ArrayList<>();
    List<QueryDocumentSnapshot> list3 = new ArrayList<>();
    ArrayList<String> cName = new ArrayList<>();
    ArrayList<String> cID = new ArrayList<>();//Also used for phone DONT ASK
    ArrayList<String> cEmail = new ArrayList<>();
    RecyclerView recyclerView;
    searchRecyclerAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        // Defines the xml file for the fragment
        return inflater.inflate(R.layout.fragment_search, parent, false);
    }

    // This event is triggered soon after onCreateView().
    // Any view setup should occur here.  E.g., view lookups and attaching view listeners.
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // Setup any handles to view objects here
        // EditText etFoo = (EditText) view.findViewById(R.id.etFoo);
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        user = mAuth.getCurrentUser();
        recyclerView = getView().findViewById(R.id.searchRecyclerView);
        Button searchButton = view.findViewById(R.id.searchButton);
        final EditText searchString = view.findViewById(R.id.searchString);

        optionSwitch = view.findViewById(R.id.donorCharitySwitch);
        optionSwitch.setChecked(true);

        initRecyclerView();

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchDatabase(searchString.getText().toString());
            }
        });

        optionSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                searchDatabase("");
            }
        });
    }

    public void searchDatabase(final String userSearchQuery){
        list.clear();
        list2.clear();
        list3.clear();
        cName.clear();
        cID.clear();
        cEmail.clear();

        final String TAG = "Search Query";
        Log.d(TAG, "lmao");
        if(optionSwitch.isChecked()){
            //Get Collection
            db.collection("Charities").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                @Override
                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                    if (task.isSuccessful()) {
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            if(document.getData().get("Name").toString().toLowerCase().contains(userSearchQuery.toLowerCase())){
                                list.add(document);
                                Log.d(TAG, document.getData().get("Name").toString());
                            }
                        }
                        for(QueryDocumentSnapshot document: list){
                            Log.d(TAG, "Here's your fucking document " + document.getData().get("Name").toString());
                            cName.add(document.getData().get("Name").toString());
                            cID.add(document.getId());

                            //THIS DOES NOTHING TO HELP CATCH DATA. THIS IS FILLING UP GARBAGE
                            cEmail.add(document.getId());
                        }
                        updateCharityData();

                    } else {
                        Log.d(TAG, "Error getting documents: ", task.getException());
                        Toast.makeText(getContext(), "An error occurred. Please check your internet connection and try again.", Toast.LENGTH_LONG).show();
                    }
                }
            });


        }
        else{
            db.collection("Charities").document(user.getUid()).collection("Users").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                @Override
                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                    if(task.isSuccessful()){
                        Log.d(TAG, "List of valid users acquired!");
                        for(QueryDocumentSnapshot document : task.getResult()){
                            list2.add(document);
                        }
                        db.collection("Users").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<QuerySnapshot> task2) {
                                if(task2.isSuccessful()){
                                    for(QueryDocumentSnapshot document : task2.getResult()){
                                        list3.add(document);
                                    }

                                    //OOF GOODBYE O(n) TIMING
                                    for(QueryDocumentSnapshot document : list3){
                                        for(QueryDocumentSnapshot document2 : list2){
                                            if(document2.getId().equals(document.getId())){
                                                list.add(document);
                                            }
                                        }
                                    }

                                    for(QueryDocumentSnapshot document : list){
                                        if(document.getString("Name").toLowerCase().contains(userSearchQuery.toLowerCase())){
                                            cName.add(document.getString("Name"));
                                            cID.add(document.getString("Phone"));
                                            cEmail.add(document.getString("Email"));
                                        }
                                    }

                                    updateDonorData();
                                }
                                else{
                                    Log.d(TAG, "Error getting document: ", task2.getException());
                                }
                            }
                        });
                    }
                    else{
                        Log.d(TAG, "Error getting document: ", task.getException());
                        Toast.makeText(getContext(), "An error occurred. Please check your internet connection and try again.", Toast.LENGTH_LONG).show();
                    }
                }
            });
        }
    }

    private void initRecyclerView(){
        searchDatabase("");

        adapter = new searchRecyclerAdapter(getContext(), cName, cID, cEmail, getFragmentManager(), true);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    public void updateCharityData(){
        adapter.setData(cName, cID, cEmail, true);
        adapter.notifyDataSetChanged();
    }

    public void updateDonorData(){
        adapter.setData(cName, cID, cEmail, false);
        adapter.notifyDataSetChanged();
    }

}
