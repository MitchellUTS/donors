package com.example.ses.navViewFragments;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.ses.R;

import java.util.ArrayList;

public class donationHistoryAdapter extends RecyclerView.Adapter<donationHistoryAdapter.ViewHolder>{


    private ArrayList<String> mCharityName;
    private Context mContext;
    private ArrayList<String> mAmount;
    private ArrayList<String> mDate;

    public donationHistoryAdapter(Context context, ArrayList<String> name, ArrayList<String> amount, ArrayList<String> date){
        mCharityName = name;
        mContext = context;
        mAmount = amount;
        mDate = date;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_history_item_donations, parent, false);
        ViewHolder holder = new ViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.charityName.setText(mCharityName.get(position));
        holder.amountPaid.setText(mAmount.get(position));
        holder.datePaid.setText(mDate.get(position));
    }

    @Override
    public int getItemCount() {
        return mCharityName.size();
    }

    public void setData(ArrayList<String> newNameList, ArrayList<String> newAmountList, ArrayList<String> newDateList){
        mCharityName = newNameList;
        mAmount = newAmountList;
        mDate = newDateList;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView charityName;
        TextView amountPaid;
        TextView datePaid;
        RelativeLayout parentLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            charityName = itemView.findViewById(R.id.nameOfCharity);
            parentLayout = itemView.findViewById(R.id.relativeLayoutDonationHistory);
            amountPaid = itemView.findViewById(R.id.amountPaid);
            datePaid = itemView.findViewById(R.id.timeDonated);
        }
    }

}
