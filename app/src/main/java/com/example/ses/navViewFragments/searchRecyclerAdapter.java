package com.example.ses.navViewFragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.ses.R;
import com.example.ses.charityInformation;

import java.util.ArrayList;

public class searchRecyclerAdapter extends RecyclerView.Adapter<searchRecyclerAdapter.ViewHolder>{


    private ArrayList<String> mName;
    private Context mContext;
    private ArrayList<String> mID;
    private FragmentManager mFragMan;
    private ArrayList<String> mEmail;
    private boolean isCharity;

    public searchRecyclerAdapter(Context context, ArrayList<String> name, ArrayList<String> id, ArrayList<String> email, FragmentManager framentMang, boolean charity){
        mName = name;
        mContext = context;
        mID = id;
        mFragMan = framentMang;
        isCharity = charity;
        mEmail = email;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_donation_history_list_item, parent, false);
        ViewHolder holder = new ViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.name.setText(mName.get(position));
        holder.charityID.setText(mID.get(position));
        holder.email.setText(mEmail.get(position));
    }

    @Override
    public int getItemCount() {
        return mName.size();
    }

    public void setData(ArrayList<String> newNameList, ArrayList<String> newIdList, boolean charity){
        mID = newIdList;
        mName = newNameList;
        isCharity = charity;
    }

    public void setData(ArrayList<String> newNameList, ArrayList<String> newPhoneList, ArrayList<String> newEmailList, boolean charity){
        mID = newPhoneList;
        mName = newNameList;
        mEmail = newEmailList;
        isCharity = charity;
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView name;
        TextView charityID;
        TextView email;
        RelativeLayout parentLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.nameOfCharity);
            parentLayout = itemView.findViewById(R.id.relativeLayout);
            charityID = itemView.findViewById(R.id.idHidden);
            email = itemView.findViewById(R.id.emailHidden);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Class fragmentClass;
            Fragment fragment = null;

            Bundle bundle = new Bundle();
            if(isCharity){
                bundle.putString("ID", charityID.getText().toString());
                fragmentClass = charityInformation.class;
            }
            else{
                bundle.putString("Phone", charityID.getText().toString());
                bundle.putString("Name", name.getText().toString());
                bundle.putString("Email", email.getText().toString());
                fragmentClass = otherProfileFragment.class;
            }

            try {
                fragment = (Fragment) fragmentClass.newInstance();
            } catch (Exception e) {
                e.printStackTrace();
            }

            fragment.setArguments(bundle);

            mFragMan.beginTransaction().replace(R.id.mainLayout, fragment).addToBackStack(null).commit();
        }
    }

}
