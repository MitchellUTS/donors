package com.example.ses.navViewFragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ses.R;
import com.example.ses.donationHistory;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

public class myProfileFragment extends Fragment implements View.OnClickListener{

    private Button donationHistoryButton;
    private FirebaseFirestore db;
    private FirebaseUser user;
    private NavigationView navView;
    private FirebaseAuth mAuth;
    private View currentView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        // Prepare the database
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        user = mAuth.getCurrentUser();
        navView = parent.getRootView().findViewById(R.id.nav_view);

        //Get the required document
        DocumentReference userDocRef = db.collection("Users").document(user.getUid());
        userDocRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            final String TAG = "retrieve User doc===";

            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        Log.d(TAG, "DocumentSnapshot data: " + document.getData());
                        setUserFields(document);
                    } else {
                        Log.d(TAG, "No such document");
                    }
                } else {
                    Log.d(TAG, "get failed with ", task.getException());
                }
            }
        });
        return inflater.inflate(R.layout.fragment_my_profile, parent, false);



    }

    // This event is triggered soon after onCreateView().
    // Any view setup should occur here.  E.g., view lookups and attaching view listeners.
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // Setup any handles to view objects here
        // EditText etFoo = (EditText) view.findViewById(R.id.etFoo);
        TextView nameField = view.findViewById(R.id.myProfileName);
        donationHistoryButton = view.findViewById(R.id.donationHistoryButton);
        Button editDetailsButton = view.findViewById(R.id.profileEditButton);
        Button editLoginButton = view.findViewById(R.id.loginEditDetails);
        Button editPasswordButton = view.findViewById(R.id.editPasswordButton);
        donationHistoryButton.setOnClickListener(this);

        currentView = view;

        //Create a prompt to edit details.
        editDetailsButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                // get the view
                LayoutInflater li = LayoutInflater.from(currentView.getContext());
                View promptsView = li.inflate(R.layout.profile_edit_dialog_box, null);

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        currentView.getContext());

                // set prompts.xml to alertdialog builder
                alertDialogBuilder.setView(promptsView);

                final EditText fullnameEditInput = promptsView
                        .findViewById(R.id.nameEditText);
                final EditText phoneNumEditInput = promptsView.findViewById(R.id.phoneEditText);

                // set dialog message
                alertDialogBuilder
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        //Update name
                                        if(!fullnameEditInput.getText().toString().equals("")){
                                            db.collection("Users").document(user.getUid()).update("Name", fullnameEditInput.getText().toString()
                                            ).addOnSuccessListener(new OnSuccessListener<Void>() {
                                                @Override
                                                public void onSuccess(Void aVoid) {
                                                    Log.d("Updating User:", "DocumentSnapshot successfully updated!");
                                                }
                                            })
                                                    .addOnFailureListener(new OnFailureListener() {
                                                        @Override
                                                        public void onFailure(@NonNull Exception e) {
                                                            Log.w("Updating User:", "Error updating document", e);
                                                            Toast.makeText(getContext(), "Error updating details, please check your connection and try again later", Toast.LENGTH_LONG).show();
                                                        }
                                                    });
                                        }
                                        //Update phone number
                                        if(!phoneNumEditInput.getText().toString().equals("")){
                                            db.collection("Users").document(user.getUid()).update("Phone", phoneNumEditInput.getText().toString()
                                            ).addOnSuccessListener(new OnSuccessListener<Void>() {
                                                @Override
                                                public void onSuccess(Void aVoid) {
                                                    Log.d("Updating User:", "DocumentSnapshot successfully updated!");
                                                    Toast.makeText(getContext(), "Details Successfully updated!", Toast.LENGTH_LONG).show();
                                                }
                                            })
                                                    .addOnFailureListener(new OnFailureListener() {
                                                        @Override
                                                        public void onFailure(@NonNull Exception e) {
                                                            Log.w("Updating User:", "Error updating document", e);
                                                            Toast.makeText(getContext(), "Error updating details, please check your connection and try again later", Toast.LENGTH_LONG).show();
                                                        }
                                                    });
                                        }
                                        //Update userfields
                                        DocumentReference userDocRef = db.collection("Users").document(user.getUid());
                                        userDocRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                            final String TAG = "retrieve User doc===";

                                            @Override
                                            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                                if (task.isSuccessful()) {
                                                    DocumentSnapshot document = task.getResult();
                                                    if (document.exists()) {
                                                        Log.d(TAG, "DocumentSnapshot data: " + document.getData());
                                                        setUserFields(document);
                                                        setNavViewFields(document);
                                                    } else {
                                                        Log.d(TAG, "No such document");
                                                    }
                                                } else {
                                                    Log.d(TAG, "get failed with ", task.getException());
                                                }
                                            }
                                        });
                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();

            }});
        editLoginButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                // get the view
                LayoutInflater li = LayoutInflater.from(currentView.getContext());
                View promptsView = li.inflate(R.layout.profile_edit_email_dialog, null);

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        currentView.getContext());

                // set prompts.xml to alertdialog builder
                alertDialogBuilder.setView(promptsView);

                final TextView emailTextView = promptsView.findViewById(R.id.emailEditText);
                final TextView emailConfirmationView = promptsView.findViewById(R.id.emailConfirmationText);

                // set dialog message
                alertDialogBuilder
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {

                                        if(!emailTextView.getText().toString().equals(emailConfirmationView.getText().toString())){
                                            Toast.makeText(getContext(), "Emails do not match", Toast.LENGTH_LONG).show();
                                        }
                                        else{
                                            user.updateEmail(emailTextView.getText().toString())
                                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                        @Override
                                                        public void onComplete(@NonNull Task<Void> task) {
                                                            if (task.isSuccessful()) {
                                                                Log.d("Change Status:", "User email address updated.");
                                                                Toast.makeText(getContext(), "Email Successully changed!", Toast.LENGTH_LONG).show();
                                                            }
                                                            else{
                                                                Log.d("Change Status:", "User email could not be updated");
                                                            }
                                                        }
                                                    });
                                            db.collection("Users").document(user.getUid()).update("Email", emailTextView.getText().toString()
                                            ).addOnSuccessListener(new OnSuccessListener<Void>() {
                                                @Override
                                                public void onSuccess(Void aVoid) {
                                                    Log.d("Change Status:", "Database email address updated.");
                                                }
                                            });
                                            TextView emailView = currentView.findViewById(R.id.myProfileEmail);
                                            emailView.setText(emailTextView.getText().toString());
                                            TextView emailHeader = navView.getHeaderView(0).findViewById(R.id.navHeaderEmail);
                                            emailHeader.setText(emailTextView.getText().toString());

                                        }


                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
            }});

        editPasswordButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                // get the view
                LayoutInflater li = LayoutInflater.from(currentView.getContext());
                View promptsView = li.inflate(R.layout.profile_edit_password_dialog, null);

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        currentView.getContext());

                // set prompts.xml to alertdialog builder
                alertDialogBuilder.setView(promptsView);

                final TextView oldPassword = promptsView.findViewById(R.id.oldPasswordTextView);
                final TextView newPassword = promptsView.findViewById(R.id.newPasswordTextView);
                final TextView newPasswordConfirmation = promptsView.findViewById(R.id.newPasswordConfirmation);

                // set dialog message
                alertDialogBuilder
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        if(newPassword.getText().toString().equals(newPasswordConfirmation.getText().toString())) {
                                            TextView emailHeader = navView.getHeaderView(0).findViewById(R.id.navHeaderEmail);
                                            AuthCredential credential = EmailAuthProvider
                                                    .getCredential(emailHeader.getText().toString(), oldPassword.getText().toString());

                                            // Prompt the user to re-provide their sign-in credentials
                                            user.reauthenticate(credential)
                                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                        @Override
                                                        public void onComplete(@NonNull Task<Void> task) {
                                                            if(task.isSuccessful()){
                                                                Log.d("Authenticating User:", "User re-authenticated.");
                                                                user.updatePassword(newPassword.getText().toString())
                                                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                            @Override
                                                                            public void onComplete(@NonNull Task<Void> task) {
                                                                                if (task.isSuccessful()) {
                                                                                    Log.d("Update password:", "User password updated.");
                                                                                    Toast.makeText(getContext(), "Password successfully changed!", Toast.LENGTH_LONG).show();
                                                                                }
                                                                                else{
                                                                                    Toast.makeText(getContext(), "Something went wrong, please check your connection and try again later.", Toast.LENGTH_LONG).show();
                                                                                }
                                                                            }
                                                                        });
                                                            }
                                                            else{
                                                                Log.d("Authenticating User:", "User failed to authenticate");
                                                            }
                                                        }
                                                    });
                                        }
                                        else{
                                            Toast.makeText(getContext(), "New Password does not match", Toast.LENGTH_LONG).show();
                                        }
                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
            }});
    }

    @Override
    public void onClick(View view) {
        Class fragmentClass = null;
        Fragment fragment = null;

        switch(view.getId()){
            case R.id.donationHistoryButton:
                fragmentClass = donationHistory.class;
                break;
        }

        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        FragmentManager fragmentManager = getFragmentManager();
        //fragmentManager.popBackStack();
        fragmentManager.beginTransaction().replace(R.id.mainLayout, fragment).addToBackStack(null).commit();
    }

    private void setUserFields(DocumentSnapshot document){
        TextView profileName = getView().findViewById(R.id.myProfileName);
        TextView profileEmail = getView().findViewById(R.id.myProfileEmail);
        TextView profilePhoneNum = getView().findViewById(R.id.myProfileAccNo);

        profileName.setText(document.getString("Name"));
        profileEmail.setText(document.getString("Email"));
        profilePhoneNum.setText(document.getString("Phone"));
    }

    private void setNavViewFields(DocumentSnapshot document){

        TextView emailHeader = navView.getHeaderView(0).findViewById(R.id.navHeaderEmail);
        TextView nameHeader = navView.getHeaderView(0).findViewById(R.id.navHeaderName);

        nameHeader.setText(document.getString("Name"));
        emailHeader.setText(document.getString("Email"));
    }
}