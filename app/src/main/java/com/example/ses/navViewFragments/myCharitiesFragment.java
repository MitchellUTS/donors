package com.example.ses.navViewFragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.ses.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

public class myCharitiesFragment extends Fragment implements View.OnClickListener {

    private String charityID;
    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private NavigationView navView;
    private View currentView;
    private FirebaseUser user;
    private String charityNa;
    private String charityWebL;
    private String charityEmail;

    private Button registerCharityButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        // Prepare the database
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        user = mAuth.getCurrentUser();
        navView = parent.getRootView().findViewById(R.id.nav_view);

        //Get the required document
        DocumentReference userDocRef = db.collection("Charities").document(user.getUid());
        userDocRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            final String TAG = "retrieve Charity doc===";

            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        Log.d(TAG, "DocumentSnapshot data: " + document.getData());
                        registerCharityButton.setEnabled(false);
                        setCharityFields(document);
                    } else {
                        Log.d(TAG, "No such document");
                    }
                } else {
                    Log.d(TAG, "get failed with ", task.getException());
                }
            }
        });
        return inflater.inflate(R.layout.fragment_charity_profile, parent, false);
    }

    // This event is triggered soon after onCreateView().
    // Any view setup should occur here.  E.g., view lookups and attaching view listeners.
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // Setup any handles to view objects here
        // EditText etFoo = (EditText) view.findViewById(R.id.etFoo);
        registerCharityButton = view.findViewById(R.id.registerButtonSig);
        registerCharityButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Class fragmentClass = null;
        Fragment fragment = null;

        switch(view.getId()){
            case R.id.registerButtonSig:
                fragmentClass = registerCharityActivity.class;
                break;
        }

        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        FragmentManager fragmentManager = getFragmentManager();
        //fragmentManager.popBackStack();
        fragmentManager.beginTransaction().replace(R.id.mainLayout, fragment).addToBackStack(null).commit();
    }

    private void setCharityFields(DocumentSnapshot document){
        TextView charityName = getView().findViewById(R.id.charityProfileName);
        TextView charityEmail = getView().findViewById(R.id.emailCharityLink);
        TextView charityWebLink = getView().findViewById(R.id.charityWebLinkButton);

        charityName.setText(document.getString("Name"));
        charityEmail.setText(document.getString("Email"));
        charityWebLink.setText(document.getString("WebLink"));
    }
}

