package com.example.ses.navViewFragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ses.R;
import com.example.ses.charityInformation;
import com.example.ses.mail.SendMailUtil;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.SetOptions;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class donationWindow extends Fragment{

    FirebaseFirestore db;
    FirebaseUser user;
    String charityID;
    String charityName;
    String mUserId;
    String donationDocRef;
    Button donationButton;
    private static final String TAG = "DonationWindow";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState){

        return inflater.inflate(R.layout.fragment_donate_amount, parent, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState){
        TextView charityNameField = view.findViewById(R.id.currentCharityString);
        Bundle bundle = this.getArguments();
        if(bundle != null){
            charityID = bundle.getString("ID");
            charityName = bundle.getString("Name");
            charityNameField.setText("Current Charity Selected: " + charityName);
        }
        else{
            charityNameField.setText("Current Charity Selected: ");
            charityID = null;
            charityName = null;
        }
        db = FirebaseFirestore.getInstance();
        user = FirebaseAuth.getInstance().getCurrentUser();
        mUserId = user.getUid();

        donationButton = view.findViewById(R.id.donateToCharity);
        donationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(charityID == null){
                    Toast.makeText(getContext(), "Please select a charity to donate to!", Toast.LENGTH_LONG).show();
                }
                else{
                    addDonation(charityID);
                }
            }
        });
        Button setCharity = view.findViewById(R.id.setCharityButton);
        setCharity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Class fragmentClass = searchFragment.class;
                Fragment fragment = null;

                try {
                    fragment = (Fragment) fragmentClass.newInstance();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.mainLayout, fragment).addToBackStack(null).commit();
            }
        });
    }

    private void addDonation(final String charityID){
        final TextView amountDonation = getView().findViewById(R.id.donationAmount);

        final Map<String,Object> donation = new HashMap<>();

        //Create Hash for database
        donation.put("Amount", Integer.parseInt(amountDonation.getText().toString()));
        donation.put("Charity", charityID);
        donation.put("Time", new Date());
        donation.put("User", mUserId);

        //Store it in the database.
        db.collection("Donations")
                .add(donation)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
//                        Toast.makeText(getContext(), "Booking completed!",
//                                Toast.LENGTH_LONG).show();
                        donationDocRef = documentReference.getId();

                        final Map<String, Object> donationHash = new HashMap<>();
                        donationHash.put("DonationID", donationDocRef);
                        donationHash.put("Charity", charityID);
                        donationHash.put("User", user.getUid());

                        DocumentReference docIdRef = db.collection("Charities").document(charityID).collection("Users").document(user.getUid());
                        docIdRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                if (task.isSuccessful()) {
                                    DocumentSnapshot document = task.getResult();
                                    if (document.exists()) {
                                        //Checked if the document exists
                                        Log.d(TAG, "Document exists!");
                                        db.collection("Charities")
                                                .document(charityID)
                                                .collection("Users")
                                                .document(user.getUid())
                                                .collection("Donations")
                                                .document(donationDocRef)
                                                .set(donationHash).addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                if(task.isSuccessful()){
                                                    Log.d(TAG, "Success1");
                                                    //donationComplete(Integer.parseInt(amountDonation.getText().toString()));
                                                }
                                                else{
                                                    Toast.makeText(getContext(), "Donation Failed, Please try again later.", Toast.LENGTH_LONG).show();
                                                }
                                            }});
                                    } else {
                                        Log.d(TAG, "Document does not exist!");
                                        db.collection("Charities").document(charityID).collection("Users").document(user.getUid())
                                                .set(new HashMap<String, Object>(), SetOptions.merge())
                                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                    @Override
                                                    public void onSuccess(Void aVoid) {
                                                        Log.d(TAG, "userPage successfully written!");
                                                        db.collection("Charities")
                                                                .document(charityID)
                                                                .collection("Users")
                                                                .document(user.getUid())
                                                                .collection("Donations")
                                                                .document(donationDocRef)
                                                                .set(donationHash).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                            @Override
                                                            public void onComplete(@NonNull Task<Void> task) {
                                                                if(task.isSuccessful()){

                                                                    Log.d(TAG, "Success2");
                                                                    donationComplete(Integer.parseInt(amountDonation.getText().toString()));
                                                                }
                                                                else{
                                                                    Toast.makeText(getContext(), "Donation Failed, Please try again later.", Toast.LENGTH_LONG).show();
                                                                }
                                                            }});
                                                    }
                                                });
                                    }
                                } else {
                                    Log.d(TAG, "Failed with: ", task.getException());
                                }
                            }
                        });

                        DocumentReference docIdRef2 = db.collection("Users")
                                .document(user.getUid())
                                .collection("Donations")
                                .document(donationDocRef);
                        docIdRef2.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                if(task.isSuccessful()){
                                    DocumentSnapshot document = task.getResult();
                                    if(document.exists()){
                                        Log.d(TAG, "Document exists in user!");
                                        db.collection("Users")
                                                .document(user.getUid())
                                                .collection("Donations")
                                                .document(donationDocRef)
                                                .set(donationHash).addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                if(task.isSuccessful()){
                                                    donationComplete(Integer.parseInt(amountDonation.getText().toString()));
                                                }
                                                else{
                                                    Toast.makeText(getContext(), "Donation Failed, Please try again later.", Toast.LENGTH_LONG).show();
                                                }
                                            }});
                                    }
                                    else{
                                        Log.d(TAG, "Document does not exist in user!");

                                        db.collection("Users")
                                                .document(user.getUid())
                                                .collection("Donations").document(donationDocRef)
                                                .set(new HashMap<String, Object>(), SetOptions.merge())
                                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                    @Override
                                                    public void onSuccess(Void aVoid) {
                                                        Log.d(TAG, "donationsFolder successfully written!");
                                                        db.collection("Users")
                                                                .document(user.getUid())
                                                                .collection("Donations")
                                                                .document(donationDocRef)
                                                                .set(donationHash).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                            @Override
                                                            public void onComplete(@NonNull Task<Void> task) {
                                                                if(task.isSuccessful()){
                                                                    donationComplete(Integer.parseInt(amountDonation.getText().toString()));
                                                                }
                                                                else{
                                                                    Toast.makeText(getContext(), "Donation Failed, Please try again later.", Toast.LENGTH_LONG).show();
                                                                }
                                                            }});
                                                    }
                                                });
                                    }
                                }
                                else{
                                    Log.d(TAG, "Failed with: ", task.getException());
                                }
                            }
                        });
                    }

                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getContext(), "Donation failed, please try again later.",
                                Toast.LENGTH_LONG).show();
                    }
                });
    }

    public void donationComplete(int amount){
        LayoutInflater li = LayoutInflater.from(getView().getContext());
        View promptsView = li.inflate(R.layout.fragment_confirm_donate, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                getView().getContext());

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);
        TextView dialog = promptsView.findViewById(R.id.confirmationMessage);
        dialog.setText("Thank you for donation of $" + amount + " to " + charityName + "!");

        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                });
        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();

        String mailText = "Dear " + user.getDisplayName() + " you have successfully made a donation of $" + amount + " to " + charityName + ". Thank you for choosing us!!";
        //    + "\nCharity:"  + "\nTitle:" + "\nTime:" + "\nDate:"
        SendMailUtil.send(user.getEmail(),"Successfully Booked",mailText);
    }
}
