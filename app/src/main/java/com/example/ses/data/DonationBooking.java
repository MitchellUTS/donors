package com.example.ses.data;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ses.R;

import com.example.ses.mail.SendMailUtil;
import com.example.ses.navViewFragments.searchFragment;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.SetOptions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DonationBooking extends DialogFragment {
    FirebaseFirestore db;
    FirebaseUser user;
    String mUserId;
    Spinner Hour,Min,Month,Date;
    String hour,min,month,date;
    private static final String TAG = "DonationBooking";
    private AutoCompleteTextView autoText;
    String bookingDocRef;
    String charityID;
    String charityName;

    Button booking_btn;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        // Defines the xml file for the fragment
        return inflater.inflate(R.layout.fragment_booking, parent, false);
    }

    // This event is triggered soon after onCreateView().
    // Any view setup should occur here.  E.g., view lookups and attaching view listeners.
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // Setup any handles to view objects here
        super.onCreate(savedInstanceState);
        TextView charityNameField = view.findViewById(R.id.currentCharityName);

        Bundle bundle = this.getArguments();
        if(bundle != null){
            charityID = bundle.getString("ID");
            charityName = bundle.getString("Name");
            charityNameField.setText("Current Charity Selected: " + charityName);
        }
        else{
            charityNameField.setText("Current Charity Selected: ");
            charityID = null;
            charityName = null;
        }

        user = FirebaseAuth.getInstance().getCurrentUser();
        mUserId = user.getUid();

        final String[] hours= new String[]{"0","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23"};
        String[] mins = new String[]{"00","10","20","30","40","50"};
        String[] months = new String[]{"1","2","3","4","5","6","7","8","9","10","11","12"};
        String[] dates = new String[]{"1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31"};
        ArrayAdapter<String> hoursAdp = new ArrayAdapter<String>(view.getContext(),android.R.layout.simple_spinner_item,hours);
        hoursAdp.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Hour = view.findViewById(R.id.booking_hour);
        Hour.setAdapter(hoursAdp);
        Hour.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                hour = Hour.getItemAtPosition(position).toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter<String> minsAdp = new ArrayAdapter<String>(view.getContext(),android.R.layout.simple_spinner_item,mins);
        minsAdp.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Min = view.findViewById(R.id.booking_mins);
        Min.setAdapter(minsAdp);
        Min.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                min = Min.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter<String> monthsAdp = new ArrayAdapter<String>(view.getContext(),android.R.layout.simple_spinner_item,months);
        monthsAdp.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Month = view.findViewById(R.id.booking_month);
        Month.setAdapter(monthsAdp);
        Month.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                month = Month.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter<String> datesAdp = new ArrayAdapter<String>(view.getContext(),android.R.layout.simple_spinner_item,dates);
        datesAdp.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Date = view.findViewById(R.id.booking_date);
        Date.setAdapter(datesAdp);
        Date.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                date = Date.getItemAtPosition(position).toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }

        });

        this.db = FirebaseFirestore.getInstance();



        booking_btn = view.findViewById(R.id.booking_btn);
        booking_btn.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceType")
            @Override
            public void onClick(View view) {
                if(charityID == null){
                    Toast.makeText(getContext(), "Please select a charity to donate to!", Toast.LENGTH_LONG).show();
                }
                else{
                    addBooking(charityID);
                }
            }
        });
        Button charitySetter = view.findViewById(R.id.setCharityButtonBook);
        charitySetter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Class fragmentClass = searchFragment.class;
                Fragment fragment = null;

                try {
                    fragment = (Fragment) fragmentClass.newInstance();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.mainLayout, fragment).addToBackStack(null).commit();
            }
        });
    }

    private void addBooking(final String charityID){
        TextView bookingTitle = getView().findViewById(R.id.booking_title);

        final Map<String,Object>booking = new HashMap<>();
        List<Integer> dateList = new ArrayList<>();
        dateList.add(Integer.parseInt(date));
        dateList.add(Integer.parseInt(month));
        List<Integer>timeList = new ArrayList<>();
        timeList.add(Integer.parseInt(hour));
        timeList.add(Integer.parseInt(min));


        booking.put("Charity", charityID);
        booking.put("Date",dateList);
        booking.put("Time",timeList);
        booking.put("User",mUserId);
        booking.put("Status",false);
        booking.put("Title", bookingTitle.getText().toString());

        db.collection("Bookings")
                .add(booking)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        bookingDocRef = documentReference.getId();

                        final Map<String, Object> bookingHash = new HashMap<>();
                        bookingHash.put("BookingID", bookingDocRef);
                        bookingHash.put("Charity", charityID);
                        bookingHash.put("User", user.getUid());

                        DocumentReference docIdRef = db.collection("Charities").document(charityID).collection("Users").document(user.getUid());
                        docIdRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                if (task.isSuccessful()) {
                                    DocumentSnapshot document = task.getResult();
                                    if (document.exists()) {
                                        //Checked if the document exists
                                        Log.d(TAG, "Document exists!");
                                        db.collection("Charities")
                                                .document(charityID)
                                                .collection("Users")
                                                .document(user.getUid())
                                                .collection("Bookings")
                                                .document(bookingDocRef)
                                                .set(bookingHash).addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                if(task.isSuccessful()){
                                                    Log.d(TAG, "Success1");
                                                }
                                                else{
                                                    Toast.makeText(getContext(), "Booking Failed, Please try again later.", Toast.LENGTH_LONG).show();
                                                }
                                            }});
                                    } else {
                                        Log.d(TAG, "Document does not exist!");
                                        db.collection("Charities").document(charityID).collection("Users").document(user.getUid())
                                                .set(new HashMap<String, Object>(), SetOptions.merge())
                                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                    @Override
                                                    public void onSuccess(Void aVoid) {
                                                        Log.d(TAG, "userPage successfully written!");
                                                        db.collection("Charities")
                                                                .document(charityID)
                                                                .collection("Users")
                                                                .document(user.getUid())
                                                                .collection("Bookings")
                                                                .document(bookingDocRef)
                                                                .set(bookingHash).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                            @Override
                                                            public void onComplete(@NonNull Task<Void> task) {
                                                                if(task.isSuccessful()){
                                                                    Log.d(TAG, "Success1");
                                                                }
                                                                else{
                                                                    Toast.makeText(getContext(), "Booking Failed, Please try again later.", Toast.LENGTH_LONG).show();
                                                                }
                                                            }});
                                                    }
                                                });
                                    }
                                } else {
                                    Log.d(TAG, "Failed with: ", task.getException());
                                }
                            }
                        });

                        DocumentReference docIdRef2 = db.collection("Users")
                                .document(user.getUid())
                                .collection("Booking")
                                .document(bookingDocRef);
                        docIdRef2.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                if(task.isSuccessful()){
                                    DocumentSnapshot document = task.getResult();
                                    if(document.exists()){
                                        Log.d(TAG, "Document exists in user!");
                                        db.collection("Users")
                                                .document(user.getUid())
                                                .collection("Bookings")
                                                .document(bookingDocRef)
                                                .set(bookingHash).addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                if(task.isSuccessful()){
                                                    bookingComplete();
                                                }
                                                else{
                                                    Toast.makeText(getContext(), "Booking Failed, Please try again later.", Toast.LENGTH_LONG).show();
                                                }
                                            }});
                                    }
                                    else{
                                        Log.d(TAG, "Document does not exist in user!");

                                        db.collection("Users")
                                                .document(user.getUid())
                                                .collection("Bookings").document(bookingDocRef)
                                                .set(new HashMap<String, Object>(), SetOptions.merge())
                                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                    @Override
                                                    public void onSuccess(Void aVoid) {
                                                        Log.d(TAG, "bookingsfolder successfully written!");
                                                        db.collection("Users")
                                                                .document(user.getUid())
                                                                .collection("Bookings")
                                                                .document(bookingDocRef)
                                                                .set(bookingHash).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                            @Override
                                                            public void onComplete(@NonNull Task<Void> task) {
                                                                if(task.isSuccessful()){
                                                                    bookingComplete();
                                                                }
                                                                else{
                                                                    Toast.makeText(getContext(), "Booking Failed, Please try again later.", Toast.LENGTH_LONG).show();
                                                                }
                                                            }});
                                                    }
                                                });
                                    }
                                }
                                else{
                                    Log.d(TAG, "Failed with: ", task.getException());
                                }
                            }
                        });
                    }

                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getContext(), "Booking failed, please try again later.",
                                Toast.LENGTH_LONG).show();
                    }
                });




    }

    public void bookingComplete(){
        LayoutInflater li = LayoutInflater.from(getView().getContext());
        View promptsView = li.inflate(R.layout.fragment_confirm_book, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                getView().getContext());

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);
        TextView dialog = promptsView.findViewById(R.id.confirmationMessasgeBook);
        dialog.setText("You have successfully booked with " + charityName + "!");

        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();

        String mailText = "Dear " + user.getDisplayName() + " you have successfully made a booking. Thank you for choosing us!!";
        //    + "\nCharity:"  + "\nTitle:" + "\nTime:" + "\nDate:"
        SendMailUtil.send(user.getEmail(),"Successfully Booked",mailText);
    }

}

