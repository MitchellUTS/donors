package com.example.ses.data;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.ses.R;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class Booking {
    private List<Integer> date;
    private String charity;
    private String user;
    private List<Integer> time;
    private boolean status;
    private String title;

    public Booking() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Integer> getDate() {
        return date;
    }

    public void setDate(List<Integer> date) {
        this.date = date;
    }

    public String getCharity() {
        return charity;
    }

    public void setCharity(String charity) {
        this.charity = charity;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public List<Integer> getTime() {
        return time;
    }

    public void setTime(List<Integer> time) {
        this.time = time;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Booking(List<Integer> date, String charity, String user, List<Integer> time, boolean status, String title) {
        this.date = date;
        this.charity = charity;
        this.user = user;
        this.time = time;
        this.status = status;
        this.title = title;
    }

    @Override
    public String toString() {
        return "Booking{" +
                "date=" + date +
                ", charity='" + charity + '\'' +
                ", user='" + user + '\'' +
                ", time=" + time +
                ", status=" + status +
                ", title='" + title + '\'' +
                '}';
    }


}

