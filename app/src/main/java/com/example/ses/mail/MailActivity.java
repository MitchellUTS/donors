package com.example.ses.mail;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.example.ses.R;


public class MailActivity extends AppCompatActivity {

    private EditText editText,subET,textET;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mail);
        editText = (EditText) findViewById(R.id.toAddEt);
        subET = findViewById(R.id.subjectET);
        textET = findViewById(R.id.textET);
    }


    public void senTextMail(View view) {
        SendMailUtil.send(editText.getText().toString(),subET.getText().toString(),textET.getText().toString());
    }




}