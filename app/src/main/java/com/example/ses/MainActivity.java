package com.example.ses;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.ses.navViewFragments.DonateAndBook;
import com.example.ses.navViewFragments.myCharitiesFragment;
import com.example.ses.navViewFragments.myProfileFragment;
import com.example.ses.navViewFragments.searchFragment;
import com.example.ses.ui.login.LoginActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout mDrawer;
    private FirebaseFirestore db;
    private FirebaseUser user;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Get database instance
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        user = mAuth.getCurrentUser();

        //Initialisation
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main); //Set Activity layout.
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mDrawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        DocumentReference userDocRef = db.collection("Users").document(user.getUid());
        userDocRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            final String TAG = "retrieve User doc===";

            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        Log.d(TAG, "DocumentSnapshot data: " + document.getData());
                        setNavViewFields(document);
                    } else {
                        Log.d(TAG, "No such document");
                    }
                } else {
                    Log.d(TAG, "get failed with ", task.getException());
                }
            }
        });

        //This is an example of how to add data to a database.
//        CollectionReference bookings = db.collection("Bookings");
//
//        Map<String, Object> data = new HashMap<>();
//        data.put("Charity", db.document("Charity/000000001"));
//        data.put("Date", Arrays.asList(12, 12, 12));
//        data.put("Status", true);
//        data.put("Time", Arrays.asList(12, 12));
//        data.put("User", db.document("Users/0000000001"));
//
//        bookings.document("0000000002").set(data);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        Fragment fragment = null;
        Class fragmentClass = null;

        switch(item.getItemId()){
            case R.id.nav_myProfile:
                fragmentClass = myProfileFragment.class;
                break;
            case R.id.nav_myCharities:
                fragmentClass = myCharitiesFragment.class;
                break;
            case R.id.nav_donate:
                fragmentClass = DonateAndBook.class;
                break;
            case R.id.nav_search:
                fragmentClass = searchFragment.class;
                break;
//            case R.id.nav_mail:
//                Intent intentMail;
//                intentMail = new Intent(MainActivity.this, MailActivity.class);
//                startActivity(intentMail);
//                break;
            case R.id.nav_map:
                Intent intentMap;
                intentMap = new Intent(MainActivity.this,MapsActivity.class);
                startActivity(intentMap);
                break;

            case R.id.nav_sign_out:
                FirebaseAuth.getInstance().signOut();
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
                break;
        }

        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        //fragmentManager.popBackStack();
        fragmentManager.beginTransaction().replace(R.id.mainLayout, fragment).addToBackStack(null).commit();

        item.setChecked(true);
        setTitle(item.getTitle());
        mDrawer.closeDrawers();
        return true;
    }

    private void setNavViewFields(DocumentSnapshot document){
        NavigationView navView = findViewById(R.id.nav_view);
        View parentView = navView.getHeaderView(0);
        TextView emailHeader = parentView.findViewById(R.id.navHeaderEmail);
        TextView nameHeader = parentView.findViewById(R.id.navHeaderName);

        nameHeader.setText(document.getString("Name"));
        emailHeader.setText(document.getString("Email"));
    }
}
