package com.example.ses;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.ses.navViewFragments.donationHistoryAdapter;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class donationHistory extends Fragment {

    FirebaseFirestore db;
    FirebaseUser user;
    FirebaseAuth mAuth;
    List<QueryDocumentSnapshot> list = new ArrayList<>();
    ArrayList<String> cName = new ArrayList<>();
    ArrayList<String> cCharityID = new ArrayList<>();
    ArrayList<String> cAmount = new ArrayList<>();
    ArrayList<String> cDate = new ArrayList<>();
    donationHistoryAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        // Defines the xml file for the fragment
        return inflater.inflate(R.layout.fragment_donation_history, parent, false);
    }

    // This event is triggered soon after onCreateView().
    // Any view setup should occur here.  E.g., view lookups and attaching view listeners.
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // Setup any handles to view objects here
        // EditText etFoo = (EditText) view.findViewById(R.id.etFoo);
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        user = mAuth.getCurrentUser();
        searchDatabase();
        initRecyclerView();
    }

    public void searchDatabase(){
        list.clear();
        cName.clear();
        cAmount.clear();
        cDate.clear();
        cCharityID.clear();

        db.collection("Donations").whereEqualTo("User", user.getUid()).get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        for(QueryDocumentSnapshot document : queryDocumentSnapshots){
                            cCharityID.add(document.getString("Charity"));
                            cAmount.add("$" + document.getData().get("Amount").toString());
                            cDate.add(document.getDate("Time").toString());
                        }
                    }
                });

        db.collection("Charities").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if(task.isSuccessful()){
                    Log.d("ayylmao","shit");
                    for(String cID: cCharityID){
                        for(QueryDocumentSnapshot document : task.getResult()){
                            if(cID.equals(document.getId())){
                                cName.add(document.getString("Name"));
                            }
                        }
                    }
                    setData();
                }
                else{
                    Toast.makeText(getContext(), "There was a problem, please check your connection and try again", Toast.LENGTH_LONG).show();
                }
            }
        });
            //        for(String cID : cCharityID){
//            db.collection("Charities").document(cID).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
//                @Override
//                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
//                    if(task.isSuccessful()){
//                        Log.d("Ayylmao", "noice");
//                        cName.add(task.getResult().getString("Name"));
//                    }
//                }
//            });
//        }
    }

    private void initRecyclerView(){
        RecyclerView recyclerView = getView().findViewById(R.id.history_recyclerView);
        adapter = new donationHistoryAdapter(getContext(), cName, cAmount, cDate );
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    public void setData(){
        adapter.setData(cName, cAmount, cDate);
        adapter.notifyDataSetChanged();
    }
}
