package com.example.ses.ui.login;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ses.MainActivity;
import com.example.ses.R;
import com.example.ses.mail.SendMailUtil;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends AppCompatActivity {

    private LoginViewModel loginViewModel;
    private FirebaseAuth mAuth;
    private FirebaseFirestore db;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        loginViewModel = ViewModelProviders.of(this, new LoginViewModelFactory())
                .get(LoginViewModel.class);

        final EditText fullnameEditText = findViewById(R.id.fullnameET);
        final EditText usernameEditText = findViewById(R.id.usernameET);
        final EditText emailEditText = findViewById(R.id.registerEmail);
        final EditText emailConfirmation = findViewById(R.id.registerEmail2);
        final EditText passwordEditText = findViewById(R.id.passwordET);
        final EditText passwordConfirmation = findViewById(R.id.passwordET2);
        final Button registerButton = findViewById(R.id.registerButton);
        final Button signInButtonActivity = findViewById(R.id.signInButton);
        final CheckBox tosCheckbox = findViewById(R.id.tosCheckbox);
        //final ProgressBar loadingProgressBar = findViewById(R.id.loading);

        db = FirebaseFirestore.getInstance();

        mAuth = FirebaseAuth.getInstance();

        signInButtonActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                Intent myIntent = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(myIntent);
                finish();
            }
        });

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isNumExistInString(fullnameEditText.getText().toString())){
                    Toast.makeText(getApplicationContext(), "Please do not use numbers for your full name.", Toast.LENGTH_LONG).show();
                    return;
                }

                if(isStringEqual(emailEditText.getText().toString(), "")){
                    Toast.makeText(getApplicationContext(), "The email field is empty", Toast.LENGTH_LONG).show();
                    return;
                }

                if(isStringEqual(passwordEditText.getText().toString(), "")){
                    Toast.makeText(getApplicationContext(), "The password field is empty", Toast.LENGTH_LONG).show();
                    return;
                }

                if(isStringEqual(emailConfirmation.getText().toString(), "")){
                    Toast.makeText(getApplicationContext(), "Please fill the email confirmation field", Toast.LENGTH_LONG).show();
                    return;
                }

                if(isStringEqual(passwordConfirmation.getText().toString(), "")){
                    Toast.makeText(getApplicationContext(), "Please fill the password confirmation field", Toast.LENGTH_LONG).show();
                    return;
                }

                if(!isStringEqual(emailEditText.getText().toString(), emailConfirmation.getText().toString())){
                    Toast.makeText(getApplicationContext(), "Emails do not match.", Toast.LENGTH_LONG).show();
                    return;
                }

                if(!isStringEqual(passwordEditText.getText().toString(), passwordConfirmation.getText().toString())){
                    Toast.makeText(getApplicationContext(), "Passwords do not match", Toast.LENGTH_LONG).show();
                    return;
                }

                if(!tosCheckbox.isChecked()){
                    Toast.makeText(getApplicationContext(), "Please read and accept the Terms of Service!", Toast.LENGTH_LONG).show();
                    return;
                }

                //Create the account after fields have been checked.
                //Switch the activity to Login once they have created the account.
                createAccount(emailEditText.getText().toString(), passwordEditText.getText().toString(), fullnameEditText.getText().toString());

                String mailText = "Dear " + fullnameEditText.getText().toString() + "You have been successfully registered. Thank you for choosing us!!";

                SendMailUtil.send(emailEditText.getText().toString(),"Successfully Registered",mailText);
            }
        });

    }

    private void updateUiWithUser(LoggedInUserView model) {
        String welcome = getString(R.string.welcome) + model.getDisplayName();
        // TODO : initiate successful logged in experience
        Toast.makeText(getApplicationContext(), welcome, Toast.LENGTH_LONG).show();
    }

    private void showLoginFailed(@StringRes Integer errorString) {
        Toast.makeText(getApplicationContext(), errorString, Toast.LENGTH_SHORT).show();
    }

    private void createAccount(String email, String password, String usersName){
        final String TAG = "createAccountFunc ===";

        //Prepare fields for innerclass.
        final String innerclassEmail = email;
        final String innerclassName = usersName;
        final Button registerButton = findViewById(R.id.registerButton);

        registerButton.setEnabled(false);

        Log.v(TAG, "Creation started");
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {

                            // Sign in success, update UI with the signed-in user's information
                            Log.v(TAG, "createUserWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();

                            //Add a user document in the cloud firestore.
                            addUserToDatabase(innerclassEmail, innerclassName, user); addUserToDatabase(innerclassEmail, innerclassName, user);

                            //Change Intent and tell the user to Login with the newly created account
                            Toast.makeText(RegisterActivity.this, "Please sign in using your newly created account", Toast.LENGTH_LONG).show();
                            changeToLoginActivity();

                        } else {
                            // If sign in fails, display a message to the user.
                            Log.v(TAG, "createUserWithEmail:failure", task.getException());
                            Toast.makeText(RegisterActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            registerButton.setEnabled(true);
                        }
                    }
                });
    }

    private boolean isNumExistInString(String input){
        char[] chars = input.toCharArray();
        for(char c : chars){
            if(Character.isDigit(c)){
                return true;
            }
        }
        return false;
    }

    private boolean isStringEqual(String s1, String s2){
        if(s1.equals(s2)){
            return true;
        }
        return false;
    }

    private void addUserToDatabase(String emailAddress, String name, FirebaseUser user){
        CollectionReference bookings = db.collection("Users");

        Log.d("what the heck", "writing to database");
        //Prepare empty maps and data map
        //data map
        Map<String, Object> data = new HashMap<>();
        //Empty maps
        Map<String, Object> bookingMap = new HashMap<>();
        Map<String, Object> donationMap = new HashMap<>();
        Map<String, Object> managesMap = new HashMap<>();

        //Put data into the map
        data.put("Bookings", bookingMap);
        data.put("Donations", donationMap);
        data.put("Email", emailAddress);
        data.put("Name", name);
        data.put("Manages", managesMap);
        data.put("Phone", "");
        data.put("Status", true);

        //send it to firebase.
        bookings.document(user.getUid()).set(data);
        Log.d("what the heck", "finished");
    }

    private void changeToLoginActivity(){
        Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private void changeToMainActivity(){
        Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
