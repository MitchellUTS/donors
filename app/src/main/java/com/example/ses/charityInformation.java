package com.example.ses;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.ses.data.DonationBooking;
import com.example.ses.navViewFragments.donationWindow;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

public class charityInformation extends Fragment{

    String charityID;
    FirebaseAuth mAuth;
    FirebaseFirestore db;
    FirebaseUser user;
    String charityNa;
    String charityWebL;
    String charityEmail;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        // Defines the xml file for the fragment

        Bundle bundle = this.getArguments();
        if(bundle != null){
            charityID = bundle.getString("ID");
        }
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        user = mAuth.getCurrentUser();

        //Get the required document
        DocumentReference userDocRef = db.collection("Charities").document(charityID);
        userDocRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            final String TAG = "retrieve Charity doc===";

            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        Log.d(TAG, "DocumentSnapshot data: " + document.getData());
                        charityNa = document.getString("Name");
                        charityWebL = document.getString("WebLink");
                        charityEmail = document.getString("Email");
                        setCharityFields();

                    } else {
                        Log.d(TAG, "No such document");
                    }
                } else {
                    Log.d(TAG, "get failed with ", task.getException());
                }
            }
        });
        return inflater.inflate(R.layout.fragment_charity_information, parent, false);

    }

    // This event is triggered soon after onCreateView().
    // Any view setup should occur here.  E.g., view lookups and attaching view listeners.
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // Setup any handles to view objects here
        Button donate = view.findViewById(R.id.donateButtonInfo);
        Button book = view.findViewById(R.id.bookButtonInfo);

        donate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = null;
                Class fragmentClass = donationWindow.class;

                Bundle bundle = new Bundle();
                bundle.putString("ID", charityID);
                bundle.putString("Name", charityNa);

                try {
                    fragment = (Fragment) fragmentClass.newInstance();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                fragment.setArguments(bundle);

                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.mainLayout, fragment).addToBackStack(null).commit();
            }
        });
        book.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = null;
                Class fragmentClass = DonationBooking.class;

                Bundle bundle = new Bundle();
                bundle.putString("ID", charityID);
                bundle.putString("Name", charityNa);

                try{
                    fragment = (Fragment) fragmentClass.newInstance();
                }
                catch(Exception e){
                    e.printStackTrace();
                }

                fragment.setArguments(bundle);

                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.mainLayout, fragment).addToBackStack(null).commit();
            }
        });
    }

    public void setCharityFields(){
        try {
            TextView charityName = getView().findViewById(R.id.charityName);
            TextView charityweblink = getView().findViewById(R.id.webLinkButton);
            TextView email = getView().findViewById(R.id.emailLink);
            charityName.setText(charityNa);
            charityweblink.setText(charityWebL);
            email.setText(charityEmail);
        }
        catch(NullPointerException e){
            e.printStackTrace();
        }

    }
}
